module random-text

go 1.14

require (
	github.com/pulumi/pulumi-gcp/sdk/v5 v5.11.0
	github.com/pulumi/pulumi-kubernetes/sdk/v3 v3.5.0
	github.com/pulumi/pulumi/sdk/v3 v3.6.1
)
