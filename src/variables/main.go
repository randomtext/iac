package variables

type Variables struct {
	GCPZone                            string
	GCloudGKEClusterName               string
	GCloudGKENumberOfNodes             string
	GCloudGKEMachineType               string
	GCloudGKEDiskSizeGBNodes           string
	GCloudGKENameSpace                 string
	GCloudGKEServiceName               string
	GCloudGKEAuthorizedNetwork         string
	GCloudGKEAuthorizedNetworkName     string
	GCloudGKEDepGuiName                string
	GCloudGKEDepGuiVolumePath          string
	GCloudGKEDepGuiVolumeCertName      string
	GCloudGKESecretName                string
	GCloudGKESecretTLS                 string
	GCloudGKESecretFullChain           string
	GCloudGKESecretPrivateKey          string
	GCloudGKESecretChain               string
	GCloudGKEIngressIPName             string
	GCloudProjectID                    string
	GCloudGKEDepGuiImage               string
	GCloudGKEDepMongoName              string
	GCloudGKEDepMongoImage             string
	GCloudGKEDepApiName                string
	GCloudGKEDepApiImage               string
	GCloudGKEDepApiVolumePath          string
	GCloudGKEDepApiVolumeCertName      string
	GCloudGKEDepRabbitMQName           string
	GCloudGKEDepRabbitMQImage          string
	GCloudGKEDepRabbitmqApiName        string
	GCloudGKEDepRabbitmqApiImage       string
	GCloudGKEDepRedisName              string
	GCloudGKEDepRedisImage             string
	GCloudGKEDepRedisApiName           string
	GCloudGKEDepRedisApiImage          string
	GCloudGKEDepRedisApiVolumePath     string
	GCloudGKEDepRedisApiVolumeCertName string
	GCloudGKEDepGrpcApiName            string
	GCloudGKEDepGrpcApiImage           string
	GCloudGKEDepGrpcApiVolumePath      string
	GCloudGKEDepGrpcApiVolumeCertName  string
	GCloudGKEIngressGrpcIPName         string
	GCloudGKEIngressGrpcName           string
}

var Vars = Variables{
	GCPZone:                            "gcp1:zone",
	GCloudGKEClusterName:               "gcp1:cluster-name",
	GCloudGKENumberOfNodes:             "gcp1:number-of-nodes",
	GCloudGKEMachineType:               "gcp1:machine-type",
	GCloudGKEDiskSizeGBNodes:           "gcp1:node-disk-size",
	GCloudGKENameSpace:                 "gcp1:name-space",
	GCloudGKEServiceName:               "gcp1:service-name",
	GCloudGKEAuthorizedNetwork:         "gcp1:authorized-network",
	GCloudGKEAuthorizedNetworkName:     "gcp1:authorized-network-name",
	GCloudGKEDepGuiName:                "gcp1:dep-gui-name",
	GCloudGKEDepGuiVolumePath:          "gcp1:dep-gui-volume-path",
	GCloudGKEDepGuiVolumeCertName:      "gcp1:dep-gui-volume-cert-name",
	GCloudGKESecretName:                "gcp1:secret-name",
	GCloudGKESecretTLS:                 "gcp1:secret-tls",
	GCloudGKESecretFullChain:           "-----BEGIN CERTIFICATE-----\nMIIFKDCCBBCgAwIBAgISA10StRLcy+fda3hQC/HOEOOEMA0GCSqGSIb3DQEBCwUA\nMDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQD\nEwJSMzAeFw0yMTA2MjYyMDQzMjFaFw0yMTA5MjQyMDQzMjBaMBwxGjAYBgNVBAMM\nESoucmFuZG9tLXRleHQub3JnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC\nAQEAzvdNMYMRT3UykaEktPKDdeutanFfeTE/XQ3AQ5llvhhgWTTD4dBajaRoe3F2\nOG5+/OtjmwkFksmjI46DWVYPIcgJ8ZE8HQo6OTLgGUpZmfT3wTabImx3vPZTbTrJ\n5p52jb+RgXKVsex12YaYHuSAQb1K0fnTDEFETrPIGYN6pkz/l05IDUCMEWgpftkl\nZKGlygo5kiWr4ajlh4DuewxPtooCXD/vYC5Bo/A60cpid/qKX4JOV46cKciwz08D\nF8YJDzfC+PK6tThFD0Xe972/wDOOoKbM28k+8zbwDc3r3D46wA5KO0fcx+5YJlqK\nQvUH9HWNfXd8ZkeT0iwO2TuawwIDAQABo4ICTDCCAkgwDgYDVR0PAQH/BAQDAgWg\nMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0G\nA1UdDgQWBBR2sDo03LbqvaYQywWLhsfhUzAaczAfBgNVHSMEGDAWgBQULrMXt1hW\ny65QCUDmH6+dixTCxjBVBggrBgEFBQcBAQRJMEcwIQYIKwYBBQUHMAGGFWh0dHA6\nLy9yMy5vLmxlbmNyLm9yZzAiBggrBgEFBQcwAoYWaHR0cDovL3IzLmkubGVuY3Iu\nb3JnLzAcBgNVHREEFTATghEqLnJhbmRvbS10ZXh0Lm9yZzBMBgNVHSAERTBDMAgG\nBmeBDAECATA3BgsrBgEEAYLfEwEBATAoMCYGCCsGAQUFBwIBFhpodHRwOi8vY3Bz\nLmxldHNlbmNyeXB0Lm9yZzCCAQQGCisGAQQB1nkCBAIEgfUEgfIA8AB2AJQgvB6O\n1Y1siHMfgosiLA3R2k1ebE+UPWHbTi9YTaLCAAABekpG8sEAAAQDAEcwRQIgKESs\nqCnksb6bmanz79HZ7Lj5KfYXiAHUzgf2BaaGJCQCIQD2lhn7YSDd0j9L0XDwcr08\nrtkGBpQ+1bd2zaCxtZZU6QB2AH0+8viP/4hVaCTCwMqeUol5K8UOeAl/LmqXaJl+\nIvDXAAABekpG86kAAAQDAEcwRQIgPravBh22RW7SYIkBsSpApaBJaHyyvPP8rAdJ\nSkiw/BwCIQDjqUoTAwaQCY0BAgKhNHDU+8ktKKMw8Qt8u8fUv3dH7TANBgkqhkiG\n9w0BAQsFAAOCAQEAjytTvshx0ElNi/dEXnFrL6M2EavBI//VPpZTt8Fg1CiubOOh\nFjyL4IfDfFMRzXlBtyQpycpo50hdbyWg+IseUVHd+twoSpkmHo78Wnkxyqj7vo9i\n5HkLns33ROwyND0UlvWrbYBZZ1Xd1XEiSFf2XcWXAi/QZMpm6+FeIAJEnI+VB9pv\nvSMp3MYFzAoM8nkuBzoXSFtF5C8ChPaFQDdoW28PfhmpBA42BeYSdJNkoxIxhf3E\nyQ9vMZ/XLoaa6Ylh5YqH01ukAzhzU3OqL/ipUNsZLVFS5qSqY4tzoKN1SJWGuzGq\n9xdR8pBTQD87biHFahChfw7cjKVD/naLfeBEdg==\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIFFjCCAv6gAwIBAgIRAJErCErPDBinU/bWLiWnX1owDQYJKoZIhvcNAQELBQAw\nTzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\ncmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMjAwOTA0MDAwMDAw\nWhcNMjUwOTE1MTYwMDAwWjAyMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNTGV0J3Mg\nRW5jcnlwdDELMAkGA1UEAxMCUjMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK\nAoIBAQC7AhUozPaglNMPEuyNVZLD+ILxmaZ6QoinXSaqtSu5xUyxr45r+XXIo9cP\nR5QUVTVXjJ6oojkZ9YI8QqlObvU7wy7bjcCwXPNZOOftz2nwWgsbvsCUJCWH+jdx\nsxPnHKzhm+/b5DtFUkWWqcFTzjTIUu61ru2P3mBw4qVUq7ZtDpelQDRrK9O8Zutm\nNHz6a4uPVymZ+DAXXbpyb/uBxa3Shlg9F8fnCbvxK/eG3MHacV3URuPMrSXBiLxg\nZ3Vms/EY96Jc5lP/Ooi2R6X/ExjqmAl3P51T+c8B5fWmcBcUr2Ok/5mzk53cU6cG\n/kiFHaFpriV1uxPMUgP17VGhi9sVAgMBAAGjggEIMIIBBDAOBgNVHQ8BAf8EBAMC\nAYYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMBIGA1UdEwEB/wQIMAYB\nAf8CAQAwHQYDVR0OBBYEFBQusxe3WFbLrlAJQOYfr52LFMLGMB8GA1UdIwQYMBaA\nFHm0WeZ7tuXkAXOACIjIGlj26ZtuMDIGCCsGAQUFBwEBBCYwJDAiBggrBgEFBQcw\nAoYWaHR0cDovL3gxLmkubGVuY3Iub3JnLzAnBgNVHR8EIDAeMBygGqAYhhZodHRw\nOi8veDEuYy5sZW5jci5vcmcvMCIGA1UdIAQbMBkwCAYGZ4EMAQIBMA0GCysGAQQB\ngt8TAQEBMA0GCSqGSIb3DQEBCwUAA4ICAQCFyk5HPqP3hUSFvNVneLKYY611TR6W\nPTNlclQtgaDqw+34IL9fzLdwALduO/ZelN7kIJ+m74uyA+eitRY8kc607TkC53wl\nikfmZW4/RvTZ8M6UK+5UzhK8jCdLuMGYL6KvzXGRSgi3yLgjewQtCPkIVz6D2QQz\nCkcheAmCJ8MqyJu5zlzyZMjAvnnAT45tRAxekrsu94sQ4egdRCnbWSDtY7kh+BIm\nlJNXoB1lBMEKIq4QDUOXoRgffuDghje1WrG9ML+Hbisq/yFOGwXD9RiX8F6sw6W4\navAuvDszue5L3sz85K+EC4Y/wFVDNvZo4TYXao6Z0f+lQKc0t8DQYzk1OXVu8rp2\nyJMC6alLbBfODALZvYH7n7do1AZls4I9d1P4jnkDrQoxB3UqQ9hVl3LEKQ73xF1O\nyK5GhDDX8oVfGKF5u+decIsH4YaTw7mP3GFxJSqv3+0lUFJoi5Lc5da149p90Ids\nhCExroL1+7mryIkXPeFM5TgO9r0rvZaBFOvV2z0gp35Z0+L4WPlbuEjN/lxPFin+\nHlUjr8gRsI3qfJOQFy/9rKIJR0Y/8Omwt/8oTWgy1mdeHmmjk7j1nYsvC9JSQ6Zv\nMldlTTKB3zhThV1+XWYp6rjd5JW1zbVWEkLNxE7GJThEUG3szgBVGP7pSWTUTsqX\nnLRbwHOoq7hHwg==\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/\nMSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\nDkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1ow\nTzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\ncmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEB\nAQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XC\nov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpL\nwYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+D\nLtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK\n4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5\nbHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5y\nsR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZ\nXmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4\nFQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBc\nSLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2ql\nPRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TND\nTwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYw\nSwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1\nc3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx\n+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEB\nATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQu\nb3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9E\nU1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26Ztu\nMA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC\n5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW\n9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuG\nWCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9O\nhe8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFC\nDfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5\n-----END CERTIFICATE-----",
	GCloudGKESecretPrivateKey:          "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDO900xgxFPdTKR\noSS08oN1661qcV95MT9dDcBDmWW+GGBZNMPh0FqNpGh7cXY4bn7862ObCQWSyaMj\njoNZVg8hyAnxkTwdCjo5MuAZSlmZ9PfBNpsibHe89lNtOsnmnnaNv5GBcpWx7HXZ\nhpge5IBBvUrR+dMMQUROs8gZg3qmTP+XTkgNQIwRaCl+2SVkoaXKCjmSJavhqOWH\ngO57DE+2igJcP+9gLkGj8DrRymJ3+opfgk5XjpwpyLDPTwMXxgkPN8L48rq1OEUP\nRd73vb/AM46gpszbyT7zNvANzevcPjrADko7R9zH7lgmWopC9Qf0dY19d3xmR5PS\nLA7ZO5rDAgMBAAECggEASzOWq+fo8UsvXbUExm7cD4vcDjI1s66OTHyDoMxrKKBo\n2eEPT8zKKm9Tu47lmumjYLkTDzE/AjAcA/5zgZGAnBC4dDsGcbLWpxPyU54tYoCd\nSWpTAjYRZ+rFM4pJ9G8durGh6Dw/Yk/+HlUGjysN41Upi5RP+EL60/IFQigA982Z\nQA3l5Lqu3On7UZeKfUmuCNRG1Zeb1l6GemqbeZp7WblYn0E51fRaSy/sGzZAmGPs\nQ5xxE35myjulgl8XvD4M4oRetAE4LFGaulSgu98EguZgYY3Zu6Gb7HP1eThXjEvB\nVbUEPfB+f5dO1UsCSEw/rtAm6QaaRCDYRgQIbklhyQKBgQDqRZZ5sZbamp1p9kz4\nTtNSVjZudQJh8jaQKly0f3ISHqOR+H07JIDSX5M8ytZVN82rBCvUftgtX77W5JLx\nI7jhIXCjm77xMbmyihYAqp20lB4R83/DGCjh53Om3cHvRAQdD6JmnCqUHzsN5WZ9\nLXy6w4zqXCGq9MuQL+pifgo5TQKBgQDiKWF7DpY6g806oWHv0gBNIC+FCz6O20kg\n8gM2jRlrLwhwXJo3iXRu5U49UwI+kRaUbmmHck9EwkshcHlqk4aMaDCzO3Jfz0QO\np/t4Vr0dM5mFUgpEU8v41Gu+9OvjboiZHCBQbVe9beMHNSPqpZ3inxH3AM9F+xl4\nr52z8U2cTwKBgQCx1dNNW67tENOPvUVedM/bv9Af9ziwy2H5+/5Vki0wbguy4hOG\nUsZra9vkGQGsvOil+kufSg6mGvlOPUMCRCCy3WSY/cBrbVjOnB3psZp3mrxc2h/R\nJETcf4r+T3ZJP7NzsAuSotd7i12TxeEQAvic+Za9zORN1vez43uj+pOePQKBgAtW\nEanOaF9UffQDZNpVh2hWRauAjckxWrAJEXophm2l9OFU8vH3tNteKRbP01v5f80+\n35FHfIuwxX1Omvdpn6PoERDa2PfOdtXZO7SjfGU7UeWp4BPes2mslB4FPtGul7A9\nBa2iXXSETo3s7vjzBj/2wzrDUqBqXrN68gez/7TnAoGAccftOKKXsI3q3QxrRrm1\nCEV+0ZKdwm0QlbRcmAUhwi6j6Cy9DynWKnDv9b+mfnmD4IXQPmavSsJvYDXMLn4S\ngSKbFwkPj89Vwbxjkv1ipVIu9uFldmh14H+cycKNvRUFVJER4BGz+oUTKJRoLg/z\nV3xIKWMyY8dCZBUqNwfN9LE=\n-----END PRIVATE KEY-----",
	GCloudGKESecretChain:               "-----BEGIN CERTIFICATE-----\nMIIFKDCCBBCgAwIBAgISA10StRLcy+fda3hQC/HOEOOEMA0GCSqGSIb3DQEBCwUA\nMDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQD\nEwJSMzAeFw0yMTA2MjYyMDQzMjFaFw0yMTA5MjQyMDQzMjBaMBwxGjAYBgNVBAMM\nESoucmFuZG9tLXRleHQub3JnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC\nAQEAzvdNMYMRT3UykaEktPKDdeutanFfeTE/XQ3AQ5llvhhgWTTD4dBajaRoe3F2\nOG5+/OtjmwkFksmjI46DWVYPIcgJ8ZE8HQo6OTLgGUpZmfT3wTabImx3vPZTbTrJ\n5p52jb+RgXKVsex12YaYHuSAQb1K0fnTDEFETrPIGYN6pkz/l05IDUCMEWgpftkl\nZKGlygo5kiWr4ajlh4DuewxPtooCXD/vYC5Bo/A60cpid/qKX4JOV46cKciwz08D\nF8YJDzfC+PK6tThFD0Xe972/wDOOoKbM28k+8zbwDc3r3D46wA5KO0fcx+5YJlqK\nQvUH9HWNfXd8ZkeT0iwO2TuawwIDAQABo4ICTDCCAkgwDgYDVR0PAQH/BAQDAgWg\nMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0G\nA1UdDgQWBBR2sDo03LbqvaYQywWLhsfhUzAaczAfBgNVHSMEGDAWgBQULrMXt1hW\ny65QCUDmH6+dixTCxjBVBggrBgEFBQcBAQRJMEcwIQYIKwYBBQUHMAGGFWh0dHA6\nLy9yMy5vLmxlbmNyLm9yZzAiBggrBgEFBQcwAoYWaHR0cDovL3IzLmkubGVuY3Iu\nb3JnLzAcBgNVHREEFTATghEqLnJhbmRvbS10ZXh0Lm9yZzBMBgNVHSAERTBDMAgG\nBmeBDAECATA3BgsrBgEEAYLfEwEBATAoMCYGCCsGAQUFBwIBFhpodHRwOi8vY3Bz\nLmxldHNlbmNyeXB0Lm9yZzCCAQQGCisGAQQB1nkCBAIEgfUEgfIA8AB2AJQgvB6O\n1Y1siHMfgosiLA3R2k1ebE+UPWHbTi9YTaLCAAABekpG8sEAAAQDAEcwRQIgKESs\nqCnksb6bmanz79HZ7Lj5KfYXiAHUzgf2BaaGJCQCIQD2lhn7YSDd0j9L0XDwcr08\nrtkGBpQ+1bd2zaCxtZZU6QB2AH0+8viP/4hVaCTCwMqeUol5K8UOeAl/LmqXaJl+\nIvDXAAABekpG86kAAAQDAEcwRQIgPravBh22RW7SYIkBsSpApaBJaHyyvPP8rAdJ\nSkiw/BwCIQDjqUoTAwaQCY0BAgKhNHDU+8ktKKMw8Qt8u8fUv3dH7TANBgkqhkiG\n9w0BAQsFAAOCAQEAjytTvshx0ElNi/dEXnFrL6M2EavBI//VPpZTt8Fg1CiubOOh\nFjyL4IfDfFMRzXlBtyQpycpo50hdbyWg+IseUVHd+twoSpkmHo78Wnkxyqj7vo9i\n5HkLns33ROwyND0UlvWrbYBZZ1Xd1XEiSFf2XcWXAi/QZMpm6+FeIAJEnI+VB9pv\nvSMp3MYFzAoM8nkuBzoXSFtF5C8ChPaFQDdoW28PfhmpBA42BeYSdJNkoxIxhf3E\nyQ9vMZ/XLoaa6Ylh5YqH01ukAzhzU3OqL/ipUNsZLVFS5qSqY4tzoKN1SJWGuzGq\n9xdR8pBTQD87biHFahChfw7cjKVD/naLfeBEdg==\n-----END CERTIFICATE-----",
	GCloudGKEIngressIPName:             "gcp1:ingress-ip-name",
	GCloudProjectID:                    "gcp1:project-id",
	GCloudGKEDepGuiImage:               "gcp1:dep-gui-image",
	GCloudGKEDepMongoName:              "gcp1:dep-mongo-name",
	GCloudGKEDepMongoImage:             "gcp1:dep-mongo-image",
	GCloudGKEDepApiName:                "gcp1:dep-api-name",
	GCloudGKEDepApiImage:               "gcp1:dep-api-image",
	GCloudGKEDepApiVolumePath:          "gcp1:dep-api-volume-path",
	GCloudGKEDepApiVolumeCertName:      "gcp1:dep-api-volume-cert-name",
	GCloudGKEDepRabbitMQName:           "gcp1:dep-rabbit-mq-name",
	GCloudGKEDepRabbitMQImage:          "gcp1:dep-rabbit-mq-image",
	GCloudGKEDepRabbitmqApiName:        "gcp1:dep-rabbit-mq-api-name",
	GCloudGKEDepRabbitmqApiImage:       "gcp1:dep-rabbit-mq-api-image",
	GCloudGKEDepRedisName:              "gcp1:dep-redis-name",
	GCloudGKEDepRedisImage:             "gcp1:dep-redis-image",
	GCloudGKEDepRedisApiName:           "gcp1:dep-redis-api-name",
	GCloudGKEDepRedisApiImage:          "gcp1:dep-redis-api-image",
	GCloudGKEDepGrpcApiName:            "gcp1:dep-grpc-api-name",
	GCloudGKEDepGrpcApiImage:           "gcp1:dep-grpc-api-image",
	GCloudGKEDepGrpcApiVolumePath:      "gcp1:dep-grpc-api-volume-path",
	GCloudGKEDepGrpcApiVolumeCertName:  "gcp1:dep-grpc-api-volume-cert-name",
	GCloudGKEIngressGrpcIPName:         "gcp1:ingress-grpc-ip-name",
	GCloudGKEIngressGrpcName:           "gcp1:ingress-grpc-name",
	GCloudGKEDepRedisApiVolumePath:     "gcp1:dep-redis-api-volume-path",
	GCloudGKEDepRedisApiVolumeCertName: "gcp1:dep-redis-api-volume-cert-name",
}
