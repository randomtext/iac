package secrets

import (
	"encoding/base64"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	v1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func SSL(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider) (*corev1.Secret, error) {
	secretName, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretName)
	clusterName, _ := ctx.GetConfig(variables.Vars.GCloudGKEClusterName)
	//secretFullChain, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretFullChain)
	secretFullChain := variables.Vars.GCloudGKESecretFullChain
	//secretPrivateKey, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretPrivateKey)
	secretPrivateKey := variables.Vars.GCloudGKESecretPrivateKey
	//secretChain, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretChain)
	secretChain := variables.Vars.GCloudGKESecretChain
	ssl, err := corev1.NewSecret(ctx, secretName, &corev1.SecretArgs{
		Metadata: v1.ObjectMetaArgs{
			ClusterName: pulumi.String(clusterName),
			Name:        pulumi.String(secretName),
			Namespace:   namespace.Metadata.Elem().Name(),
		},
		Data: pulumi.StringMap{
			"cert.crt":  pulumi.String(base64.StdEncoding.EncodeToString([]byte(secretFullChain))),
			"cert.key":  pulumi.String(base64.StdEncoding.EncodeToString([]byte(secretPrivateKey))),
			"chain.crt": pulumi.String(base64.StdEncoding.EncodeToString([]byte(secretChain))),
		},
		Kind: pulumi.String("Opaque"),
		Type: pulumi.String("secret"),
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{k8sProvider}))
	if err != nil {
		return nil, err
	}
	return ssl, nil
}
func TLS(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider) (*corev1.Secret, error) {
	clusterName, _ := ctx.GetConfig(variables.Vars.GCloudGKEClusterName)
	secretTLS, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretTLS)
	//secretFullChain, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretFullChain)
	secretFullChain := variables.Vars.GCloudGKESecretFullChain
	//secretPrivateKey, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretPrivateKey)
	secretPrivateKey := variables.Vars.GCloudGKESecretPrivateKey
	ssl, err := corev1.NewSecret(ctx, secretTLS, &corev1.SecretArgs{
		Metadata: v1.ObjectMetaArgs{
			ClusterName: pulumi.String(clusterName),
			Name:        pulumi.String(secretTLS),
			Namespace:   namespace.Metadata.Elem().Name(),
		},
		Data: pulumi.StringMap{
			"tls.crt": pulumi.String(base64.StdEncoding.EncodeToString([]byte(secretFullChain))),
			"tls.key": pulumi.String(base64.StdEncoding.EncodeToString([]byte(secretPrivateKey))),
		},
		Kind: pulumi.String("Secret"),
		Type: pulumi.String("kubernetes.io/tls"),
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{k8sProvider}))
	if err != nil {
		return nil, err
	}
	return ssl, nil
}
