package rabbitMQ

import (
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func Deployment(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider) (*appsv1.Deployment, error) {
	rabbitmqName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRabbitMQName)
	rabbitmqImage, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRabbitMQImage)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(rabbitmqName),
	}
	rabbitmq, err := appsv1.NewDeployment(ctx, rabbitmqName, &appsv1.DeploymentArgs{
		Kind: pulumi.String("deployment"),
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(rabbitmqName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: appsv1.DeploymentSpecArgs{
			Selector: &metav1.LabelSelectorArgs{
				MatchLabels: appLabels,
			},
			Replicas: pulumi.Int(1),
			Strategy: appsv1.DeploymentStrategyArgs{
				RollingUpdate: appsv1.RollingUpdateDeploymentArgs{
					MaxSurge:       pulumi.Int(1),
					MaxUnavailable: pulumi.Int(0),
				},
				Type: pulumi.String("RollingUpdate"),
			},
			Template: &corev1.PodTemplateSpecArgs{
				Metadata: &metav1.ObjectMetaArgs{
					Labels: appLabels,
				},
				Spec: &corev1.PodSpecArgs{
					Containers: corev1.ContainerArray{
						corev1.ContainerArgs{
							Name:            pulumi.String(rabbitmqName),
							Image:           pulumi.String(rabbitmqImage),
							ImagePullPolicy: pulumi.String("Always"),
							SecurityContext: corev1.SecurityContextArgs{
								AllowPrivilegeEscalation: pulumi.BoolPtr(false),
								Privileged:               pulumi.BoolPtr(false),
								ReadOnlyRootFilesystem:   pulumi.BoolPtr(false),
								RunAsNonRoot:             pulumi.BoolPtr(false),
							},
							Stdin:                    pulumi.BoolPtr(true),
							TerminationMessagePath:   pulumi.String("/dev/termination-log"),
							TerminationMessagePolicy: pulumi.String("File"),
							Tty:                      pulumi.BoolPtr(true),
						}},
				},
			},
		},
	}, pulumi.Provider(k8sProvider))
	if err != nil {
		return nil, err
	}
	err = service(ctx, namespace, k8sProvider, rabbitmq)
	if err != nil {
		return nil, err
	}
	return rabbitmq, nil
}

func service(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, rabbitmq *appsv1.Deployment) error {
	rabbitmqName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRabbitMQName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(rabbitmqName),
	}
	_, err := corev1.NewService(ctx, rabbitmqName, &corev1.ServiceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(rabbitmqName),
			Namespace: namespace.Metadata.Elem().Name(),
			Labels:    appLabels,
			//Annotations: pulumi.StringMap{"cloud.google.com/app-protocols": pulumi.String(`{"` + guiName + `": "HTTPS"}`)},
		},
		Spec: &corev1.ServiceSpecArgs{
			Ports: corev1.ServicePortArray{
				corev1.ServicePortArgs{
					Port:       pulumi.Int(5672),
					Protocol:   pulumi.String("TCP"),
					TargetPort: pulumi.Int(5672),
					Name:       pulumi.String(rabbitmqName),
				},
			},
			Selector: appLabels,
			Type:     pulumi.String("ClusterIP"),
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{rabbitmq}))
	if err != nil {
		return err
	}
	return nil
}
