package gui

import (
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func Deployment(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, ssl *corev1.Secret) (*appsv1.Deployment, error) {
	guiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGuiName)
	guiImage, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGuiImage)
	guiVolumePath, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGuiVolumePath)
	guiVolumeCertName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGuiVolumeCertName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(guiName),
	}
	gui, err := appsv1.NewDeployment(ctx, guiName, &appsv1.DeploymentArgs{
		Kind: pulumi.String("deployment"),
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(guiName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: appsv1.DeploymentSpecArgs{
			Selector: &metav1.LabelSelectorArgs{
				MatchLabels: appLabels,
			},
			Replicas: pulumi.Int(1),
			Strategy: appsv1.DeploymentStrategyArgs{
				RollingUpdate: appsv1.RollingUpdateDeploymentArgs{
					MaxSurge:       pulumi.Int(1),
					MaxUnavailable: pulumi.Int(0),
				},
				Type: pulumi.String("RollingUpdate"),
			},
			Template: &corev1.PodTemplateSpecArgs{
				Metadata: &metav1.ObjectMetaArgs{
					Labels: appLabels,
				},
				Spec: &corev1.PodSpecArgs{
					Volumes: corev1.VolumeArray{corev1.VolumeArgs{
						Name: pulumi.String(guiVolumeCertName),
						Secret: corev1.SecretVolumeSourceArgs{
							DefaultMode: pulumi.Int(256),
							Optional:    pulumi.Bool(false),
							SecretName:  pulumi.String(guiVolumeCertName),
						},
					}},
					Containers: corev1.ContainerArray{
						corev1.ContainerArgs{
							Env: corev1.EnvVarArray{
								corev1.EnvVarArgs{
									Name:  pulumi.String("API_URL"),
									Value: pulumi.String("https://api.random-text.org/graphql"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("SUBSCRIPTION_URL"),
									Value: pulumi.String("https://api.random-text.org/subscriptions"),
								},
							},
							Name:            pulumi.String(guiName),
							Image:           pulumi.String(guiImage),
							ImagePullPolicy: pulumi.String("Always"),
							SecurityContext: corev1.SecurityContextArgs{
								AllowPrivilegeEscalation: pulumi.BoolPtr(false),
								Privileged:               pulumi.BoolPtr(false),
								ReadOnlyRootFilesystem:   pulumi.BoolPtr(false),
								RunAsNonRoot:             pulumi.BoolPtr(false),
							},
							Stdin:                    pulumi.BoolPtr(true),
							TerminationMessagePath:   pulumi.String("/dev/termination-log"),
							TerminationMessagePolicy: pulumi.String("File"),
							Tty:                      pulumi.BoolPtr(true),
							VolumeMounts: corev1.VolumeMountArray{
								corev1.VolumeMountArgs{
									MountPath: pulumi.String(guiVolumePath),
									Name:      pulumi.String(guiVolumeCertName),
								},
							},
						}},
				},
			},
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{ssl}))
	if err != nil {
		return nil, err
	}
	err = service(ctx, namespace, k8sProvider, gui)
	if err != nil {
		return nil, err
	}
	return gui, nil
}

func service(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, gui *appsv1.Deployment) error {
	guiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGuiName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(guiName),
	}
	_, err := corev1.NewService(ctx, guiName, &corev1.ServiceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name:        pulumi.String(guiName),
			Namespace:   namespace.Metadata.Elem().Name(),
			Labels:      appLabels,
			Annotations: pulumi.StringMap{"cloud.google.com/app-protocols": pulumi.String(`{"` + guiName + `": "HTTPS"}`)},
		},
		Spec: &corev1.ServiceSpecArgs{
			Ports: corev1.ServicePortArray{
				corev1.ServicePortArgs{
					Port:       pulumi.Int(443),
					Protocol:   pulumi.String("TCP"),
					TargetPort: pulumi.Int(443),
					Name:       pulumi.String(guiName),
				},
			},
			Selector: appLabels,
			Type:     pulumi.String("NodePort"),
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{gui}))
	if err != nil {
		return err
	}
	return nil
}
