package mongo

import (
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func Deployment(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider) (*appsv1.Deployment, error) {
	mongoName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepMongoName)
	mongoImage, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepMongoImage)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(mongoName),
	}
	mongo, err := appsv1.NewDeployment(ctx, mongoName, &appsv1.DeploymentArgs{
		Kind: pulumi.String("deployment"),
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(mongoName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: appsv1.DeploymentSpecArgs{
			Selector: &metav1.LabelSelectorArgs{
				MatchLabels: appLabels,
			},
			Replicas: pulumi.Int(1),
			Strategy: appsv1.DeploymentStrategyArgs{
				RollingUpdate: appsv1.RollingUpdateDeploymentArgs{
					MaxSurge:       pulumi.Int(1),
					MaxUnavailable: pulumi.Int(0),
				},
				Type: pulumi.String("RollingUpdate"),
			},
			Template: &corev1.PodTemplateSpecArgs{
				Metadata: &metav1.ObjectMetaArgs{
					Labels: appLabels,
				},
				Spec: &corev1.PodSpecArgs{
					Containers: corev1.ContainerArray{
						corev1.ContainerArgs{
							Args: pulumi.StringArray{
								pulumi.String("--replSet"),
								pulumi.String("rs0"),
							},
							Name:            pulumi.String(mongoName),
							Image:           pulumi.String(mongoImage),
							ImagePullPolicy: pulumi.String("Always"),
							SecurityContext: corev1.SecurityContextArgs{
								AllowPrivilegeEscalation: pulumi.BoolPtr(false),
								Privileged:               pulumi.BoolPtr(false),
								ReadOnlyRootFilesystem:   pulumi.BoolPtr(false),
								RunAsNonRoot:             pulumi.BoolPtr(false),
							},
							Stdin:                    pulumi.BoolPtr(true),
							TerminationMessagePath:   pulumi.String("/dev/termination-log"),
							TerminationMessagePolicy: pulumi.String("File"),
							Tty:                      pulumi.BoolPtr(true),
						}},
				},
			},
		},
	}, pulumi.Provider(k8sProvider))
	if err != nil {
		return nil, err
	}
	err = service(ctx, namespace, k8sProvider, mongo)
	if err != nil {
		return nil, err
	}
	return mongo, nil
}

func service(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, mongo *appsv1.Deployment) error {
	mongoName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepMongoName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(mongoName),
	}
	_, err := corev1.NewService(ctx, mongoName, &corev1.ServiceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(mongoName),
			Namespace: namespace.Metadata.Elem().Name(),
			Labels:    appLabels,
			//Annotations: pulumi.StringMap{"cloud.google.com/app-protocols": pulumi.String(`{"` + guiName + `": "HTTPS"}`)},
		},
		Spec: &corev1.ServiceSpecArgs{
			Ports: corev1.ServicePortArray{
				corev1.ServicePortArgs{
					Port:       pulumi.Int(27017),
					Protocol:   pulumi.String("TCP"),
					TargetPort: pulumi.Int(27017),
					Name:       pulumi.String(mongoName),
				},
			},
			Selector: appLabels,
			Type:     pulumi.String("ClusterIP"),
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{mongo}))
	if err != nil {
		return err
	}
	return nil
}
