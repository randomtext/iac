package grpc_api

import (
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func Deployment(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, ssl *corev1.Secret, mongoDep *appsv1.Deployment) (*appsv1.Deployment, error) {
	grpcApiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGrpcApiName)
	grpcApiImage, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGrpcApiImage)
	grpcApiVolumePath, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGrpcApiVolumePath)
	grpcApiVolumeCertName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGrpcApiVolumeCertName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(grpcApiName),
	}
	redisApiDep, err := appsv1.NewDeployment(ctx, grpcApiName, &appsv1.DeploymentArgs{
		Kind: pulumi.String("deployment"),
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(grpcApiName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: appsv1.DeploymentSpecArgs{
			Selector: &metav1.LabelSelectorArgs{
				MatchLabels: appLabels,
			},
			Replicas: pulumi.Int(1),
			Strategy: appsv1.DeploymentStrategyArgs{
				RollingUpdate: appsv1.RollingUpdateDeploymentArgs{
					MaxSurge:       pulumi.Int(1),
					MaxUnavailable: pulumi.Int(0),
				},
				Type: pulumi.String("RollingUpdate"),
			},
			Template: &corev1.PodTemplateSpecArgs{
				Metadata: &metav1.ObjectMetaArgs{
					Labels: appLabels,
				},
				Spec: &corev1.PodSpecArgs{
					Volumes: corev1.VolumeArray{corev1.VolumeArgs{
						Name: pulumi.String(grpcApiVolumeCertName),
						Secret: corev1.SecretVolumeSourceArgs{
							DefaultMode: pulumi.Int(256),
							Optional:    pulumi.Bool(false),
							SecretName:  pulumi.String(grpcApiVolumeCertName),
						},
					}},
					Containers: corev1.ContainerArray{
						corev1.ContainerArgs{
							Env: corev1.EnvVarArray{
								corev1.EnvVarArgs{
									Name:  pulumi.String("ENVIRONMENT"),
									Value: pulumi.String("production"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("VERSION"),
									Value: pulumi.String("v1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("SERVER_ID"),
									Value: pulumi.String("1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("CERT_FILE"),
									Value: pulumi.String("/certs/cert.crt"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("KEY_FILE"),
									Value: pulumi.String("/certs/cert.key"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("DATABASE"),
									Value: pulumi.String("random"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("MONGO_URL"),
									Value: pulumi.String("mongodb://10.112.8.9:27017/"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("GRPC_PORT"),
									Value: pulumi.String("443"),
								},
							},
							Name:            pulumi.String(grpcApiName),
							Image:           pulumi.String(grpcApiImage),
							ImagePullPolicy: pulumi.String("Always"),
							SecurityContext: corev1.SecurityContextArgs{
								AllowPrivilegeEscalation: pulumi.BoolPtr(false),
								Privileged:               pulumi.BoolPtr(false),
								ReadOnlyRootFilesystem:   pulumi.BoolPtr(false),
								RunAsNonRoot:             pulumi.BoolPtr(false),
							},
							Stdin:                    pulumi.BoolPtr(true),
							TerminationMessagePath:   pulumi.String("/dev/termination-log"),
							TerminationMessagePolicy: pulumi.String("File"),
							Tty:                      pulumi.BoolPtr(true),
							VolumeMounts: corev1.VolumeMountArray{
								corev1.VolumeMountArgs{
									MountPath: pulumi.String(grpcApiVolumePath),
									Name:      pulumi.String(grpcApiVolumeCertName),
								},
							},
						}},
				},
			},
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{ssl, mongoDep}))
	if err != nil {
		return nil, err
	}
	err = service(ctx, namespace, k8sProvider, redisApiDep)
	if err != nil {
		return nil, err
	}
	return redisApiDep, nil
}

func service(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, grpcApiDep *appsv1.Deployment) error {
	grpcApiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepGrpcApiName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(grpcApiName),
	}
	_, err := corev1.NewService(ctx, grpcApiName, &corev1.ServiceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name:        pulumi.String(grpcApiName),
			Namespace:   namespace.Metadata.Elem().Name(),
			Labels:      appLabels,
			Annotations: pulumi.StringMap{"cloud.google.com/app-protocols": pulumi.String(`{"` + grpcApiName + `": "HTTP2"}`)},
		},
		Spec: &corev1.ServiceSpecArgs{
			Ports: corev1.ServicePortArray{
				corev1.ServicePortArgs{
					Port:       pulumi.Int(443),
					Protocol:   pulumi.String("TCP"),
					TargetPort: pulumi.Int(443),
					Name:       pulumi.String(grpcApiName),
				},
			},
			Selector: appLabels,
			Type:     pulumi.String("NodePort"),
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{grpcApiDep}))
	if err != nil {
		return err
	}
	return nil
}
