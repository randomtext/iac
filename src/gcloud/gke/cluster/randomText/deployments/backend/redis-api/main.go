package redis_api

import (
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func Deployment(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, ssl *corev1.Secret, mongoDep, redisDep *appsv1.Deployment) (*appsv1.Deployment, error) {
	redisApiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRedisApiName)
	redisApiImage, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRedisApiImage)
	redisApiVolumePath, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRedisApiVolumePath)
	redisApiVolumeCertName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRedisApiVolumeCertName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(redisApiName),
	}
	redisApiDep, err := appsv1.NewDeployment(ctx, redisApiName, &appsv1.DeploymentArgs{
		Kind: pulumi.String("deployment"),
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(redisApiName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: appsv1.DeploymentSpecArgs{
			Selector: &metav1.LabelSelectorArgs{
				MatchLabels: appLabels,
			},
			Replicas: pulumi.Int(1),
			Strategy: appsv1.DeploymentStrategyArgs{
				RollingUpdate: appsv1.RollingUpdateDeploymentArgs{
					MaxSurge:       pulumi.Int(1),
					MaxUnavailable: pulumi.Int(0),
				},
				Type: pulumi.String("RollingUpdate"),
			},
			Template: &corev1.PodTemplateSpecArgs{
				Metadata: &metav1.ObjectMetaArgs{
					Labels: appLabels,
				},
				Spec: &corev1.PodSpecArgs{
					Volumes: corev1.VolumeArray{corev1.VolumeArgs{
						Name: pulumi.String(redisApiVolumeCertName),
						Secret: corev1.SecretVolumeSourceArgs{
							DefaultMode: pulumi.Int(256),
							Optional:    pulumi.Bool(false),
							SecretName:  pulumi.String(redisApiVolumeCertName),
						},
					}},
					Containers: corev1.ContainerArray{
						corev1.ContainerArgs{
							Env: corev1.EnvVarArray{
								corev1.EnvVarArgs{
									Name:  pulumi.String("ENVIRONMENT"),
									Value: pulumi.String("production"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("VERSION"),
									Value: pulumi.String("v1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("SERVER_ID"),
									Value: pulumi.String("1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("CERT_FILE"),
									Value: pulumi.String("/certs/cert.crt"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("DATABASE"),
									Value: pulumi.String("random"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("MONGO_URL"),
									Value: pulumi.String("mongodb://10.112.8.9:27017/"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("REDIS_HOST"),
									Value: pulumi.String("10.112.9.14"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("REDIS_PORT"),
									Value: pulumi.String("6379"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("GRPC_URI"),
									Value: pulumi.String("grpc-api.random-text.org:443"),
								},
							},
							Name:            pulumi.String(redisApiName),
							Image:           pulumi.String(redisApiImage),
							ImagePullPolicy: pulumi.String("Always"),
							SecurityContext: corev1.SecurityContextArgs{
								AllowPrivilegeEscalation: pulumi.BoolPtr(false),
								Privileged:               pulumi.BoolPtr(false),
								ReadOnlyRootFilesystem:   pulumi.BoolPtr(false),
								RunAsNonRoot:             pulumi.BoolPtr(false),
							},
							Stdin:                    pulumi.BoolPtr(true),
							TerminationMessagePath:   pulumi.String("/dev/termination-log"),
							TerminationMessagePolicy: pulumi.String("File"),
							Tty:                      pulumi.BoolPtr(true),
							VolumeMounts: corev1.VolumeMountArray{
								corev1.VolumeMountArgs{
									MountPath: pulumi.String(redisApiVolumePath),
									Name:      pulumi.String(redisApiVolumeCertName),
								},
							},
						}},
				},
			},
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{mongoDep, ssl, redisDep}))
	if err != nil {
		return nil, err
	}
	err = service(ctx, namespace, k8sProvider, redisApiDep)
	if err != nil {
		return nil, err
	}
	return redisApiDep, nil
}

func service(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, redisApiDep *appsv1.Deployment) error {
	redisApiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRedisApiName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(redisApiName),
	}
	_, err := corev1.NewService(ctx, redisApiName, &corev1.ServiceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(redisApiName),
			Namespace: namespace.Metadata.Elem().Name(),
			Labels:    appLabels,
			//Annotations: pulumi.StringMap{"cloud.google.com/app-protocols": pulumi.String(`{"` + redisApiName + `": "HTTPS"}`)},
		},
		Spec: &corev1.ServiceSpecArgs{
			Ports: corev1.ServicePortArray{
				corev1.ServicePortArgs{
					Port:       pulumi.Int(443),
					Protocol:   pulumi.String("TCP"),
					TargetPort: pulumi.Int(443),
					Name:       pulumi.String(redisApiName),
				},
			},
			Selector: appLabels,
			Type:     pulumi.String("NodePort"),
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{redisApiDep}))
	if err != nil {
		return err
	}
	return nil
}
