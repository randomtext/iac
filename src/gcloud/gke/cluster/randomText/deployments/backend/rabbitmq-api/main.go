package rabbitmq_api

import (
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func Deployment(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, mongoDep, rabbitmqDep, redisDep *appsv1.Deployment) (*appsv1.Deployment, error) {
	rabbitmqApiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRabbitmqApiName)
	rabbitmqApiImage, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRabbitmqApiImage)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(rabbitmqApiName),
	}
	rabbitmqApiDep, err := appsv1.NewDeployment(ctx, rabbitmqApiName, &appsv1.DeploymentArgs{
		Kind: pulumi.String("deployment"),
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(rabbitmqApiName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: appsv1.DeploymentSpecArgs{
			Selector: &metav1.LabelSelectorArgs{
				MatchLabels: appLabels,
			},
			Replicas: pulumi.Int(1),
			Strategy: appsv1.DeploymentStrategyArgs{
				RollingUpdate: appsv1.RollingUpdateDeploymentArgs{
					MaxSurge:       pulumi.Int(1),
					MaxUnavailable: pulumi.Int(0),
				},
				Type: pulumi.String("RollingUpdate"),
			},
			Template: &corev1.PodTemplateSpecArgs{
				Metadata: &metav1.ObjectMetaArgs{
					Labels: appLabels,
				},
				Spec: &corev1.PodSpecArgs{
					Containers: corev1.ContainerArray{
						corev1.ContainerArgs{
							Env: corev1.EnvVarArray{
								corev1.EnvVarArgs{
									Name:  pulumi.String("ENVIRONMENT"),
									Value: pulumi.String("production"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("VERSION"),
									Value: pulumi.String("v1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("SERVER_ID"),
									Value: pulumi.String("1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("DATABASE"),
									Value: pulumi.String("random"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("RABBITMQ_USER"),
									Value: pulumi.String("guest"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("RABBITMQ_PASSWORD"),
									Value: pulumi.String("guest"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("RABBITMQ_HOST"),
									Value: pulumi.String("10.112.8.8"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("RABBITMQ_PORT"),
									Value: pulumi.String("5672"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("MONGO_URL"),
									Value: pulumi.String("mongodb://10.112.8.9:27017/"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("REDIS_HOST"),
									Value: pulumi.String("10.112.9.14"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("REDIS_PORT"),
									Value: pulumi.String("6379"),
								},
							},
							Name:            pulumi.String(rabbitmqApiName),
							Image:           pulumi.String(rabbitmqApiImage),
							ImagePullPolicy: pulumi.String("Always"),
							SecurityContext: corev1.SecurityContextArgs{
								AllowPrivilegeEscalation: pulumi.BoolPtr(false),
								Privileged:               pulumi.BoolPtr(false),
								ReadOnlyRootFilesystem:   pulumi.BoolPtr(false),
								RunAsNonRoot:             pulumi.BoolPtr(false),
							},
							Stdin:                    pulumi.BoolPtr(true),
							TerminationMessagePath:   pulumi.String("/dev/termination-log"),
							TerminationMessagePolicy: pulumi.String("File"),
							Tty:                      pulumi.BoolPtr(true),
						}},
				},
			},
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{mongoDep, rabbitmqDep, redisDep}))
	if err != nil {
		return nil, err
	}
	err = service(ctx, namespace, k8sProvider, rabbitmqApiDep)
	if err != nil {
		return nil, err
	}
	return rabbitmqApiDep, nil
}

func service(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, rabbitmqApiDep *appsv1.Deployment) error {
	rabbitmqApiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepRabbitmqApiName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(rabbitmqApiName),
	}
	_, err := corev1.NewService(ctx, rabbitmqApiName, &corev1.ServiceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(rabbitmqApiName),
			Namespace: namespace.Metadata.Elem().Name(),
			Labels:    appLabels,
			//Annotations: pulumi.StringMap{"cloud.google.com/app-protocols": pulumi.String(`{"` + rabbitmqApiName + `": "HTTPS"}`)},
		},
		Spec: &corev1.ServiceSpecArgs{
			Ports: corev1.ServicePortArray{
				corev1.ServicePortArgs{
					Port:       pulumi.Int(443),
					Protocol:   pulumi.String("TCP"),
					TargetPort: pulumi.Int(443),
					Name:       pulumi.String(rabbitmqApiName),
				},
			},
			Selector: appLabels,
			Type:     pulumi.String("NodePort"),
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{rabbitmqApiDep}))
	if err != nil {
		return err
	}
	return nil
}
