package api

import (
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func Deployment(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, ssl *corev1.Secret, mongoDep, rabbitmqDep *appsv1.Deployment) (*appsv1.Deployment, error) {
	apiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepApiName)
	apiImage, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepApiImage)
	apiVolumePath, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepApiVolumePath)
	apiVolumeCertName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepApiVolumeCertName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(apiName),
	}
	api, err := appsv1.NewDeployment(ctx, apiName, &appsv1.DeploymentArgs{
		Kind: pulumi.String("deployment"),
		Metadata: &metav1.ObjectMetaArgs{
			Name:      pulumi.String(apiName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: appsv1.DeploymentSpecArgs{
			Selector: &metav1.LabelSelectorArgs{
				MatchLabels: appLabels,
			},
			Replicas: pulumi.Int(1),
			Strategy: appsv1.DeploymentStrategyArgs{
				RollingUpdate: appsv1.RollingUpdateDeploymentArgs{
					MaxSurge:       pulumi.Int(1),
					MaxUnavailable: pulumi.Int(0),
				},
				Type: pulumi.String("RollingUpdate"),
			},
			Template: &corev1.PodTemplateSpecArgs{
				Metadata: &metav1.ObjectMetaArgs{
					Labels: appLabels,
				},
				Spec: &corev1.PodSpecArgs{
					Volumes: corev1.VolumeArray{corev1.VolumeArgs{
						Name: pulumi.String(apiVolumeCertName),
						Secret: corev1.SecretVolumeSourceArgs{
							DefaultMode: pulumi.Int(256),
							Optional:    pulumi.Bool(false),
							SecretName:  pulumi.String(apiVolumeCertName),
						},
					}},
					Containers: corev1.ContainerArray{
						corev1.ContainerArgs{
							Env: corev1.EnvVarArray{
								corev1.EnvVarArgs{
									Name:  pulumi.String("ENVIRONMENT"),
									Value: pulumi.String("production"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("PORT"),
									Value: pulumi.String("443"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("CORS"),
									Value: pulumi.String("https://gui.random-text.org"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("VERSION"),
									Value: pulumi.String("v1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("SERVER_ID"),
									Value: pulumi.String("1"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("DATABASE"),
									Value: pulumi.String("random"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("FILE_PATH"),
									Value: pulumi.String("/certs/cert.crt"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("KEY_PATH"),
									Value: pulumi.String("/certs/cert.key"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("RABBITMQ_USER"),
									Value: pulumi.String("guest"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("RABBITMQ_PASSWORD"),
									Value: pulumi.String("guest"),
								},
								corev1.EnvVarArgs{
									Name: pulumi.String("RABBITMQ_HOST"),
									//Value: pulumi.String("rabbitmq.random-text.svc.cluster.local"),
									Value: pulumi.String("10.112.8.8"),
								},
								corev1.EnvVarArgs{
									Name:  pulumi.String("RABBITMQ_PORT"),
									Value: pulumi.String("5672"),
								},
								corev1.EnvVarArgs{
									Name: pulumi.String("MONGO_URL"),
									//Value: pulumi.String("mongodb://mongo.random-text.svc.cluster.local:27017/"),
									Value: pulumi.String("mongodb://10.112.8.9:27017/"),
								},
							},
							Name:            pulumi.String(apiName),
							Image:           pulumi.String(apiImage),
							ImagePullPolicy: pulumi.String("Always"),
							SecurityContext: corev1.SecurityContextArgs{
								AllowPrivilegeEscalation: pulumi.BoolPtr(false),
								Privileged:               pulumi.BoolPtr(false),
								ReadOnlyRootFilesystem:   pulumi.BoolPtr(false),
								RunAsNonRoot:             pulumi.BoolPtr(false),
							},
							Stdin:                    pulumi.BoolPtr(true),
							TerminationMessagePath:   pulumi.String("/dev/termination-log"),
							TerminationMessagePolicy: pulumi.String("File"),
							Tty:                      pulumi.BoolPtr(true),
							VolumeMounts: corev1.VolumeMountArray{
								corev1.VolumeMountArgs{
									MountPath: pulumi.String(apiVolumePath),
									Name:      pulumi.String(apiVolumeCertName),
								},
							},
						}},
				},
			},
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{ssl, mongoDep, rabbitmqDep}))
	if err != nil {
		return nil, err
	}
	err = service(ctx, namespace, k8sProvider, api)
	if err != nil {
		return nil, err
	}
	return api, nil
}

func service(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, api *appsv1.Deployment) error {
	apiName, _ := ctx.GetConfig(variables.Vars.GCloudGKEDepApiName)
	appLabels := pulumi.StringMap{
		"app": pulumi.String(apiName),
	}
	_, err := corev1.NewService(ctx, apiName, &corev1.ServiceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name:        pulumi.String(apiName),
			Namespace:   namespace.Metadata.Elem().Name(),
			Labels:      appLabels,
			Annotations: pulumi.StringMap{"cloud.google.com/app-protocols": pulumi.String(`{"` + apiName + `": "HTTPS"}`)},
		},
		Spec: &corev1.ServiceSpecArgs{
			Ports: corev1.ServicePortArray{
				corev1.ServicePortArgs{
					Port:       pulumi.Int(443),
					Protocol:   pulumi.String("TCP"),
					TargetPort: pulumi.Int(443),
					Name:       pulumi.String(apiName),
				},
			},
			Selector: appLabels,
			Type:     pulumi.String("NodePort"),
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{api}))
	if err != nil {
		return err
	}
	return nil
}
