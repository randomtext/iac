package randomText

import (
	"github.com/pulumi/pulumi-gcp/sdk/v5/go/gcp/container"
	"github.com/pulumi/pulumi-gcp/sdk/v5/go/gcp/projects"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
	"strconv"
)

func CreateCluster(ctx *pulumi.Context) (*container.Cluster, error) {
	serviceClusterName, _ := ctx.GetConfig(variables.Vars.GCloudGKEServiceName)
	containerService, err := projects.NewService(ctx, serviceClusterName, &projects.ServiceArgs{
		Service: pulumi.String("container.googleapis.com"),
	})
	if err != nil {
		ctx.Log.Info("service error", nil)
		ctx.Log.Info(err.Error(), nil)
		return nil, err
	}
	masterVersion := "1.19.9-gke.1400"
	clusterName, _ := ctx.GetConfig(variables.Vars.GCloudGKEClusterName)
	ctx.Log.Info(clusterName, nil)
	nodesNumberString, _ := ctx.GetConfig(variables.Vars.GCloudGKENumberOfNodes)
	ctx.Log.Info(nodesNumberString, nil)
	nodesNumber, _ := strconv.Atoi(nodesNumberString)
	zone, _ := ctx.GetConfig(variables.Vars.GCPZone)
	ctx.Log.Info(zone, nil)
	machineType, _ := ctx.GetConfig(variables.Vars.GCloudGKEMachineType)
	ctx.Log.Info(machineType, nil)
	diskSizeString, _ := ctx.GetConfig(variables.Vars.GCloudGKEDiskSizeGBNodes)
	ctx.Log.Info(diskSizeString, nil)
	diskSize, _ := strconv.Atoi(diskSizeString)

	authorizedNetwork, _ := ctx.GetConfig(variables.Vars.GCloudGKEAuthorizedNetwork)
	ctx.Log.Info(authorizedNetwork, nil)
	authorizedNetworkName, _ := ctx.GetConfig(variables.Vars.GCloudGKEAuthorizedNetworkName)
	ctx.Log.Info(authorizedNetworkName, nil)

	cluster, err := container.NewCluster(ctx, clusterName, &container.ClusterArgs{
		Name:             pulumi.String(clusterName),
		MinMasterVersion: pulumi.String(masterVersion),
		NodeVersion:      pulumi.String(masterVersion),
		InitialNodeCount: pulumi.Int(nodesNumber),

		MasterAuth: container.ClusterMasterAuthArgs{
			ClientCertificateConfig: container.ClusterMasterAuthClientCertificateConfigArgs{IssueClientCertificate: pulumi.Bool(false)},
			Password:                pulumi.String(""),
			Username:                pulumi.String(""),
		},

		NodeConfig: &container.ClusterNodeConfigArgs{
			MachineType: pulumi.String(machineType),
			DiskSizeGb:  pulumi.Int(diskSize),
			OauthScopes: pulumi.StringArray{
				pulumi.String("https://www.googleapis.com/auth/compute"),
				pulumi.String("https://www.googleapis.com/auth/devstorage.read_only"),
				pulumi.String("https://www.googleapis.com/auth/logging.write"),
				pulumi.String("https://www.googleapis.com/auth/monitoring"),
			},
		},
		Location: pulumi.String(zone),
		// --no-enable-master-authorized-networks
		MasterAuthorizedNetworksConfig: container.ClusterMasterAuthorizedNetworksConfigArgs{CidrBlocks: container.ClusterMasterAuthorizedNetworksConfigCidrBlockArray{
			container.ClusterMasterAuthorizedNetworksConfigCidrBlockArgs{
				CidrBlock:   pulumi.String(authorizedNetwork),
				DisplayName: pulumi.String(authorizedNetworkName),
			},
		}},
	}, pulumi.DependsOn([]pulumi.Resource{containerService}))
	if err != nil {
		return nil, err
	}
	ctx.Export("kubeconfig", generateKubeConfig(cluster.Endpoint, cluster.Name, cluster.MasterAuth))
	return cluster, nil
}

func CreateProvider(ctx *pulumi.Context, cluster *container.Cluster) (*providers.Provider, error) {
	k8sProvider, err := providers.NewProvider(
		ctx, "k8sprovider", &providers.ProviderArgs{
			Kubeconfig: generateKubeConfig(cluster.Endpoint, cluster.Name, cluster.MasterAuth),
		}, pulumi.DependsOn([]pulumi.Resource{cluster}))
	if err != nil {
		return nil, err
	}
	return k8sProvider, nil
}

func CreateNameSpace(ctx *pulumi.Context, k8sProvider *providers.Provider) (*corev1.Namespace, error) {
	nsName, _ := ctx.GetConfig(variables.Vars.GCloudGKENameSpace)
	namespace, err := corev1.NewNamespace(ctx, nsName, &corev1.NamespaceArgs{
		Metadata: &metav1.ObjectMetaArgs{Name: pulumi.String(nsName)},
	}, pulumi.Provider(k8sProvider))
	if err != nil {
		return nil, err
	}
	return namespace, nil
}

func generateKubeConfig(clusterEndpoint pulumi.StringOutput, clusterName pulumi.StringOutput,
	clusterMasterAuth container.ClusterMasterAuthOutput) pulumi.StringOutput {
	context := pulumi.Sprintf("%s", clusterName)

	return pulumi.Sprintf(`apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: %s
    server: https://%s
  name: %s
contexts:
- context:
    cluster: %s
    user: %s
  name: %s
current-context: %s
kind: Config
preferences: {}
users:
- name: %s
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{.credential.token_expiry}'
        token-key: '{.credential.access_token}'
      name: gcp`,
		clusterMasterAuth.ClusterCaCertificate().Elem(),
		clusterEndpoint, context, context, context, context, context, context)
}
