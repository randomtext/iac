package ingress

import (
	"github.com/pulumi/pulumi-gcp/sdk/v5/go/gcp/compute"
	v12 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/core/v1"
	v1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/meta/v1"
	networkingv1 "github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/networking/v1"
	"github.com/pulumi/pulumi-kubernetes/sdk/v3/go/kubernetes/providers"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/variables"
)

func New(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, guiDep, apiDep *v12.Deployment, tls *corev1.Secret, staticIP *compute.GlobalAddress) error {
	ingressIpName, _ := ctx.GetConfig(variables.Vars.GCloudGKEIngressIPName)
	secretTls, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretTLS)
	ingress, err := networkingv1.NewIngress(ctx, "ingress", &networkingv1.IngressArgs{
		Kind: pulumi.String("Ingress"),
		Metadata: v1.ObjectMetaArgs{
			Annotations: pulumi.StringMap{
				"kubernetes.io/ingress.allow-http":            pulumi.String("false"),
				"kubernetes.io/ingress.global-static-ip-name": pulumi.String(ingressIpName),
			},
			Name:      pulumi.String("ingress"),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: networkingv1.IngressSpecArgs{
			Rules: networkingv1.IngressRuleArray{
				networkingv1.IngressRuleArgs{
					Host: pulumi.String("gui.random-text.org"),
					Http: networkingv1.HTTPIngressRuleValueArgs{Paths: networkingv1.HTTPIngressPathArray{
						networkingv1.HTTPIngressPathArgs{
							Backend: networkingv1.IngressBackendArgs{
								Service: networkingv1.IngressServiceBackendArgs{
									Name: pulumi.String("gui"),
									Port: networkingv1.ServiceBackendPortArgs{
										Number: pulumi.Int(443),
									},
								},
							},
							Path:     pulumi.String("/*"),
							PathType: pulumi.String("ImplementationSpecific"),
						},
					}},
				},
				networkingv1.IngressRuleArgs{
					Host: pulumi.String("api.random-text.org"),
					Http: networkingv1.HTTPIngressRuleValueArgs{Paths: networkingv1.HTTPIngressPathArray{
						networkingv1.HTTPIngressPathArgs{
							Backend: networkingv1.IngressBackendArgs{
								Service: networkingv1.IngressServiceBackendArgs{
									Name: pulumi.String("api"),
									Port: networkingv1.ServiceBackendPortArgs{
										Number: pulumi.Int(443),
									},
								},
							},
							Path:     pulumi.String("/*"),
							PathType: pulumi.String("ImplementationSpecific"),
						},
					}},
				},
			},
			Tls: networkingv1.IngressTLSArray{
				networkingv1.IngressTLSArgs{
					Hosts: pulumi.StringArray{
						pulumi.String("gui.random-text.org"),
						pulumi.String("api.random-text.org"),
					},
					SecretName: pulumi.String(secretTls),
				},
			},
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{tls, staticIP, guiDep, apiDep}))
	if err != nil {
		return err
	}
	urlMap, err := compute.NewURLMap(ctx, "http-https", &compute.URLMapArgs{
		DefaultUrlRedirect: compute.URLMapDefaultUrlRedirectArgs{
			HostRedirect:         pulumi.String(""),
			HttpsRedirect:        pulumi.Bool(true),
			PrefixRedirect:       pulumi.String(""),
			RedirectResponseCode: pulumi.String("MOVED_PERMANENTLY_DEFAULT"),
			StripQuery:           pulumi.Bool(false),
		},
		Name: pulumi.String("http-https"),
	}, pulumi.DependsOn([]pulumi.Resource{ingress}))
	if err != nil {
		return err
	}
	httpProxy, err := compute.NewTargetHttpProxy(ctx, "proxy", &compute.TargetHttpProxyArgs{
		Description: pulumi.String("http proxy"),
		UrlMap:      urlMap.ID(),
	})
	if err != nil {
		return err
	}
	_, err = compute.NewGlobalForwardingRule(ctx, "fwr-http-https", &compute.GlobalForwardingRuleArgs{
		IpAddress: staticIP.Address,
		PortRange: pulumi.String("80"),
		Target:    httpProxy.ID(),
	})
	if err != nil {
		return err
	}
	return nil
}

func NewGrpc(ctx *pulumi.Context, namespace *corev1.Namespace, k8sProvider *providers.Provider, grpcApiDep *v12.Deployment, tls *corev1.Secret, staticIP *compute.GlobalAddress) error {
	ingressGrpcIpName, _ := ctx.GetConfig(variables.Vars.GCloudGKEIngressGrpcIPName)
	ingressGrpcName, _ := ctx.GetConfig(variables.Vars.GCloudGKEIngressGrpcName)
	secretTls, _ := ctx.GetConfig(variables.Vars.GCloudGKESecretTLS)
	ingressGrpc, err := networkingv1.NewIngress(ctx, ingressGrpcName, &networkingv1.IngressArgs{
		Kind: pulumi.String("Ingress"),
		Metadata: v1.ObjectMetaArgs{
			Annotations: pulumi.StringMap{
				"kubernetes.io/ingress.allow-http":            pulumi.String("false"),
				"kubernetes.io/ingress.global-static-ip-name": pulumi.String(ingressGrpcIpName),
				"nginx.ingress.kubernetes.io/grpc-backend":    pulumi.String("true"),
			},
			Name:      pulumi.String(ingressGrpcName),
			Namespace: namespace.Metadata.Elem().Name(),
		},
		Spec: networkingv1.IngressSpecArgs{
			Rules: networkingv1.IngressRuleArray{
				networkingv1.IngressRuleArgs{
					Host: pulumi.String("grpc-api.random-text.org"),
					Http: networkingv1.HTTPIngressRuleValueArgs{Paths: networkingv1.HTTPIngressPathArray{
						networkingv1.HTTPIngressPathArgs{
							Backend: networkingv1.IngressBackendArgs{
								Service: networkingv1.IngressServiceBackendArgs{
									Name: pulumi.String("grpc-api"),
									Port: networkingv1.ServiceBackendPortArgs{
										Number: pulumi.Int(443),
									},
								},
							},
							Path:     pulumi.String("/*"),
							PathType: pulumi.String("ImplementationSpecific"),
						},
					}},
				},
			},
			Tls: networkingv1.IngressTLSArray{
				networkingv1.IngressTLSArgs{
					Hosts: pulumi.StringArray{
						pulumi.String("grpc-api.random-text.org"),
					},
					SecretName: pulumi.String(secretTls),
				},
			},
		},
	}, pulumi.Provider(k8sProvider), pulumi.DependsOn([]pulumi.Resource{tls, staticIP, grpcApiDep}))
	if err != nil {
		return err
	}
	urlMap, err := compute.NewURLMap(ctx, "http-https-grpc", &compute.URLMapArgs{
		DefaultUrlRedirect: compute.URLMapDefaultUrlRedirectArgs{
			HostRedirect:         pulumi.String(""),
			HttpsRedirect:        pulumi.Bool(true),
			PrefixRedirect:       pulumi.String(""),
			RedirectResponseCode: pulumi.String("MOVED_PERMANENTLY_DEFAULT"),
			StripQuery:           pulumi.Bool(false),
		},
		Name: pulumi.String("http-https-grpc"),
	}, pulumi.DependsOn([]pulumi.Resource{ingressGrpc}))
	if err != nil {
		return err
	}
	httpProxy, err := compute.NewTargetHttpProxy(ctx, "proxy-grpc", &compute.TargetHttpProxyArgs{
		Description: pulumi.String("http proxy"),
		UrlMap:      urlMap.ID(),
	})
	if err != nil {
		return err
	}
	_, err = compute.NewGlobalForwardingRule(ctx, "fwr-http-https-grpc", &compute.GlobalForwardingRuleArgs{
		IpAddress: staticIP.Address,
		PortRange: pulumi.String("80"),
		Target:    httpProxy.ID(),
	})
	if err != nil {
		return err
	}
	return nil
}

func StaticIP(ctx *pulumi.Context, k8sProvider *providers.Provider) (*compute.GlobalAddress, error) {
	ingressIpName, _ := ctx.GetConfig(variables.Vars.GCloudGKEIngressIPName)
	projectID, _ := ctx.GetConfig(variables.Vars.GCloudProjectID)
	staticIP, err := compute.NewGlobalAddress(ctx, ingressIpName, &compute.GlobalAddressArgs{
		Name:    pulumi.String(ingressIpName),
		Project: pulumi.String(projectID),
	}, pulumi.DependsOn([]pulumi.Resource{k8sProvider}))
	if err != nil {
		return nil, err
	}
	return staticIP, nil
}

func StaticIPGrpc(ctx *pulumi.Context, k8sProvider *providers.Provider) (*compute.GlobalAddress, error) {
	ingressGrpcIpName, _ := ctx.GetConfig(variables.Vars.GCloudGKEIngressGrpcIPName)
	projectID, _ := ctx.GetConfig(variables.Vars.GCloudProjectID)
	staticIP, err := compute.NewGlobalAddress(ctx, ingressGrpcIpName, &compute.GlobalAddressArgs{
		Name:    pulumi.String(ingressGrpcIpName),
		Project: pulumi.String(projectID),
	}, pulumi.DependsOn([]pulumi.Resource{k8sProvider}))
	if err != nil {
		return nil, err
	}
	return staticIP, nil
}
