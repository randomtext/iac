package main

import (
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"random-text/src/gcloud/gke/cluster/randomText"
	"random-text/src/gcloud/gke/cluster/randomText/deployments/backend/api"
	grpc_api "random-text/src/gcloud/gke/cluster/randomText/deployments/backend/grpc-api"
	rabbitmq_api "random-text/src/gcloud/gke/cluster/randomText/deployments/backend/rabbitmq-api"
	redis_api "random-text/src/gcloud/gke/cluster/randomText/deployments/backend/redis-api"
	"random-text/src/gcloud/gke/cluster/randomText/deployments/frontend/gui"
	"random-text/src/gcloud/gke/cluster/randomText/deployments/mongo"
	"random-text/src/gcloud/gke/cluster/randomText/deployments/rabbitMQ"
	"random-text/src/gcloud/gke/cluster/randomText/deployments/redis"
	"random-text/src/gcloud/gke/cluster/randomText/ingress"
	"random-text/src/gcloud/gke/cluster/randomText/secrets"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		cluster, err := randomText.CreateCluster(ctx)
		if err != nil {
			return err
		}
		k8sProvider, err := randomText.CreateProvider(ctx, cluster)
		if err != nil {
			return err
		}
		nameSpace, err := randomText.CreateNameSpace(ctx, k8sProvider)
		if err != nil {
			return err
		}
		ssl, err := secrets.SSL(ctx, nameSpace, k8sProvider)
		if err != nil {
			return err
		}
		tls, err := secrets.TLS(ctx, nameSpace, k8sProvider)
		if err != nil {
			return err
		}
		guiDep, err := gui.Deployment(ctx, nameSpace, k8sProvider, ssl)
		if err != nil {
			return err
		}
		mongoDep, err := mongo.Deployment(ctx, nameSpace, k8sProvider)
		if err != nil {
			return err
		}
		rabbitmqDep, err := rabbitMQ.Deployment(ctx, nameSpace, k8sProvider)
		if err != nil {
			return err
		}
		apiDep, err := api.Deployment(ctx, nameSpace, k8sProvider, ssl, mongoDep, rabbitmqDep)
		if err != nil {
			return err
		}
		redisDep, err := redis.Deployment(ctx, nameSpace, k8sProvider)
		if err != nil {
			return err
		}
		_, err = rabbitmq_api.Deployment(ctx, nameSpace, k8sProvider, mongoDep, rabbitmqDep, redisDep)
		if err != nil {
			return err
		}
		_, err = redis_api.Deployment(ctx, nameSpace, k8sProvider, ssl, mongoDep, redisDep)
		if err != nil {
			return err
		}
		grpcApiDep, err := grpc_api.Deployment(ctx, nameSpace, k8sProvider, ssl, mongoDep)
		if err != nil {
			return err
		}
		staticIPGrpc, err := ingress.StaticIPGrpc(ctx, k8sProvider)
		if err != nil {
			return err
		}
		err = ingress.NewGrpc(ctx, nameSpace, k8sProvider, grpcApiDep, tls, staticIPGrpc)
		if err != nil {
			return err
		}
		staticIP, err := ingress.StaticIP(ctx, k8sProvider)
		if err != nil {
			return err
		}
		err = ingress.New(ctx, nameSpace, k8sProvider, guiDep, apiDep, tls, staticIP)
		if err != nil {
			return err
		}
		return nil
	})
}
